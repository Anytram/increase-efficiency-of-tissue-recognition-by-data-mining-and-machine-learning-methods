# Increase efficiency of tissue recognition by data mining and machine learning methods

_Michał Kruczkowski, Paweł Rudnicki, Martyna Tarczewska, Monika Kosowska, Anna Drabik-Kruczkowska, Małgorzata Szczerska_

As refractive index constitutes one of the most significant parameters describing materials, a wide variety of applications in many branches of science and technology apply its measurements. Methods based on refractive index determination can be found in chemical and biological sciences, food and pharmaceutical industries, medicine, quality control and more. Hence, there is a need for development of methods allowing its precise determination with high resolution.  
The solution includes data pre-treatment leading to obtaining feature vectors creating datasets for machine learning. The selection of the algorithm best suited for the task, data preprocessing and regression approach are described. The XGB regression algorithm was applied. The results of cross-validation show scores of 0.99 for the training and validations sets, the maximum standard deviation reached only 0.01 proving a high accuracy of the model predictions. The model was then used with new data to create data mimicking the experimental signals for samples that were not possible to measure in the laboratory, hence, extending the resolution of acquired results. This could be crucial for increasing efficiency of tissue recognition based on refractive index measurements due to greater accuracy of data. 

The best option to run this code is to upload notebook and all data (test.csv, train.csv) to Google Colaboratory, that way there are no additional steps required.
If you want to run it in your local environment you should install following packeges:

- seaborn
- pandas
- numpy
- tensorflow
- matplotlib
- scipy
- sklearn
